public class Endereco {
    private String cep;
    private String bairro;
    private String estado;
    private String cidade;
    public Endereco(EnderecoSerializado enderecoSerializado) {
        this.cep = enderecoSerializado.cep();
        this.bairro = enderecoSerializado.bairro();
        this.cidade = enderecoSerializado.localidade();
        this.estado = enderecoSerializado.uf();

    }

    @Override
    public String toString() {
        return "Estado: " + this.estado + "\n"+
                "Cidade: " + this.cidade + "\n" +
                "Bairro: " + this.bairro;
    }
}
