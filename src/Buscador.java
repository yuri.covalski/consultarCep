import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Buscador {
    public static void main(String[] args) throws IOException {
        Scanner leitura = new Scanner(System.in);
        System.out.println("Digite o cep no formato xxxxxxxx: ");
        String cep = leitura.next();
        ConsultaCep consultaCep = new ConsultaCep();


        String enderecoEncontrado = "não";
        while (Objects.equals(enderecoEncontrado, "não")){
            try {
                EnderecoSerializado enderecoSerializado = consultaCep.buscaEndereco(cep);
                Endereco endereco = new Endereco(enderecoSerializado);

                System.out.println("Endereço encontrado: \n");
                System.out.println(endereco);

                System.out.println("Este é o endereço desejado?" + "\n");
                System.out.println("(Digite 'sim' para prosseguir e 'não' para tentar novamente)");
                enderecoEncontrado = leitura.next();

                if (Objects.equals(enderecoEncontrado, "sim")) {
                    GeradorDeArquivo gerador = new GeradorDeArquivo();
                    gerador.geraJson(enderecoSerializado);
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

    }
}